import React from 'react'

const blockHeight = 40;
const blockWidth = 40;
const width = window.innerWidth;
const height =  window.innerHeight;


const randumCoordonate = (x,y,blockWidth,blockHeight) => {
      const coordonate = [];
      const coordonateX = [0,x-blockWidth];
      const coordonateY = [0,y-blockHeight];
      const randomX = coordonateX[Math.floor(coordonateX.length * Math.random())];
      const randomY = coordonateY[Math.floor(coordonateY.length * Math.random())];
      coordonate.push(randomX,randomY);
      return coordonate
}

const randomMouv = (x,y) => {
  var mouvX = Math.floor(Math.random() * (10 - 2 +1)) + 2;
  if(x > parseInt(window.innerWidth / 2)) {
    mouvX *= (-1);
  }
  var mouvY = -(Math.floor(Math.random() * (10 - 2 +1)) + 2);
  if(y > parseInt(window.innerHeight / 2)) {
    mouvY *= (-1);
  }
  return [mouvX, mouvY];
}



export default class Asteroide extends React.Component{
  constructor(props){
      super(props)
      const coord = randumCoordonate(width,height,blockWidth,blockHeight)

      this.state = {
          x: coord[0],
          y: coord[1],
          mouvX : parseInt(randomMouv(coord[0], coord[1])),
          mouvY : parseInt(randomMouv(coord[0], coord[1])),
          key: null,
        }
    }

    componentDidMount() {
        this.drawAsteroide()

        setInterval(() => {
         const asteroide = this.props.asteroideCanvas;
         asteroide.fillStyle = "#c0392b";
         var newX = this.state.x + this.state.mouvX
         var newY = this.state.y + this.state.mouvY

         if(newX >= width  || newX <= 0 ) {
           this.setState({
             mouvX: this.state.mouvX * (-1)
           })
           newX = this.state.x + this.state.mouvX
         }
         else if(newY >= height  || newY <= 0 ) {
           this.setState({
             mouvY: this.state.mouvY * (-1)
           })
           newY = this.state.y + this.state.mouvY
         }
         asteroide.clearRect(this.state.x, this.state.y, blockWidth, blockHeight);
         asteroide.fillRect(newX,newY,blockWidth,blockHeight);

         this.setState({
           x:newX,
           y:newY,
           key: this.props.id
         })

         this.props.asteroide(this.state.x,this.state.y,this.state.key);
         if(this.props.idDelete.length > 0){
         for(var l=0; l<this.props.idDelete.length; l++){
            if(this.props.id===this.props.idDelete[l]){
              asteroide.clearRect(this.props.deleteShip[0], this.props.deleteShip[1], blockWidth, blockHeight);
         }
        }
      }

      }, 20)
    }

    drawAsteroide() {
        const asteroide = this.props.asteroideCanvas;
        asteroide.fillStyle = "#c0392b";
        asteroide.fillRect(this.state.x,this.state.y,blockWidth,blockHeight);
    }



    render() {

      return(
          <div> </div>
      )
    }

}
