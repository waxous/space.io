import React from 'react'

function rect(props) {
    const {ctx, x, y, width, height} = props;
    ctx.fillRect(x, y, width, height);
}

function bullet(props) {
    const {ctx, x, y, width, height} = props;
    ctx.fillRect(x, y, width, height);
}

export default class Ship extends React.Component{

  constructor(props) {
      super(props)

      this.state = {
          screen: {
              width: window.innerWidth,
              height: window.innerHeight,
              ratio: window.devicePixelRatio || 1
          },
          ship: {
              rotation: 0,
              speed: 6,
              x: parseInt(window.innerWidth / 2),
              y: parseInt(window.innerHeight / 2),
          },
          keys: {
              left: false,
              right: false,
              up: false,
              down: false,
              space: false
          },
          lastKey: 38,
          lastShot: Math.round(new Date().getTime()),
          bullets: [],
          shot: 0,
      }
  }

  updateShip = () => {
      setInterval(() => {
        if(this.props.didLoose !== true){
          this.updateCanvas()
          this.updateBullet()
        }
      }, 1);
  }

  componentDidMount() {
      if(this.props.didLoose !== true){
      this.updateShip()
      window.addEventListener('keyup', this.handleKey.bind(this, false));
      window.addEventListener('keydown', this.handleKey.bind(this, true));
      }
  }

  componentWillUnmount() {
      if(this.props.didLoose !== true){
      window.removeEventListener('keyup', this.handleKey);
      window.removeEventListener('keydown', this.handleKey);
    }
  }

  updateCanvas() {
      const ctx = this.props.shipCanvas;
      let ship = this.state.ship
      let keys = this.state.keys;

      ctx.clearRect(parseInt(ship.x-3), parseInt(ship.y-3), 30, 30);
      ctx.fillStyle = this.state.keys.space ? "#FF0000" : '#FFF';

      if (this.state.keys.space){
        this.shotBullet()
      }
      if (ship.rotation >= 360){
          ship.rotation -= 360;
      }
      if (ship.rotation < 0) {
          ship.rotation += 360;
      }
      if(this.state.ship.x > this.state.screen.width){
          ship.x = 0;
      }
      else if(this.state.ship.x < 0){
          ship.x = this.state.screen.width;
      }
      if(this.state.ship.y > this.state.screen.height){
          ship.y = 0;
      }
      else if(this.state.ship.y < 0){
          ship.y = this.state.screen.height;
      }
      this.setState({
          ship: {
              rotation: ship.rotation,
              speed: ship.speed,
              x: ship.x + (keys.left ? -ship.speed : 0) + (keys.right ? ship.speed : 0),
              y: ship.y + (keys.up ? -ship.speed : 0) + (keys.down ? ship.speed : 0),
          }
      });

      this.props.ship(this.state.ship.x,this.state.ship.y);
      rect({ctx, x: ship.x, y: ship.y, width: 20, height: 20});
  }


  updateBullet() {
        let bullets = []
        const ctx = this.props.shipCanvas;

        for (let i = 0; i < this.state.bullets.length; i++) {
            let bull = this.state.bullets[i];
            ctx.clearRect(bull.x - 2, bull.y - 2, 8, 8);
            let x = bull.x + (bull.direction === 37 ? -2.5 : 0) + (bull.direction === 39 ? 2.5 : 0)
            let y = bull.y + (bull.direction === 38 ? -2.5 : 0) + (bull.direction === 40 ? 2.5 : 0)
            bull = {x, y, direction: bull.direction}
            bullets.push(bull)
            bullet({ctx, x, y, width: 4, height: 4});
            if(x > window.innerWidth || x < 0 || y > window.innerHeight || y < 0) {
               bullets.splice(i, 1)
               ctx.clearRect(x, y,  4,  4)
             }
        }
        this.setState({
            bullets: bullets
        })
        this.props.bullet(this.state.bullets)
    }

    shotBullet() {
    if (Math.round(new Date().getTime()) - this.state.lastShot > 500) {
        this.setState({
            shot: this.state.shot + 1
        })
        this.setState({
            lastShot: Math.round(new Date().getTime())
        })
        const ctx = this.props.shipCanvas;
        ctx.fillStyle = '#FF0000'
        let ship = this.state.ship;
        let x = ship.x + 8
        let y = ship.y + 8
        let bull = {x, y, direction: this.state.lastKey}
        bullet({ctx, x, y, width: 4, height: 4});
        let bullets = this.state.bullets.slice()
        bullets.push(bull)
        this.setState({bullets})
    }
}


  handleKey(isPressed, e) {
      let keys = this.state.keys;
      let ship = this.state.ship
      let rotation = 0

      if (e.keyCode === 32) {
          keys.space = isPressed
      }
      else this.setState({
          lastKey: e.keyCode
      });
      if (e.keyCode === 37){
          rotation-=0.1
          keys.left = isPressed
      }
      if (e.keyCode === 39){
          rotation+=0.1
          keys.right = isPressed
      }
      if (e.keyCode === 38){
          keys.up = isPressed
      }
      if (e.keyCode === 40){
          keys.down = isPressed
      }
      this.setState({
          ship: {
              rotation: parseFloat(ship.rotation + rotation),
              speed: 2,
              x: ship.x,
              y: ship.y,
          }
      })
      this.setState({keys: keys});
  }

  render() {
      return (
          <div>
          </div>
      )
  }
  }
