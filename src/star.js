import React from 'react'

const width = window.innerWidth;
const height =  window.innerHeight;

export default class Star extends React.Component{

  constructor(props){
      super(props)
      this.state = {
          stars: []
      }
    }

  componentDidMount(){
    this.drawStars()
    setInterval(() => {
      const stars = this.props.starCanvas;
      stars.fillStyle = "#FFF";
      stars.clearRect(this.state.stars[0],this.state.stars[1],this.state.stars[2],this.state.stars[3]);
      stars.fillRect(this.state.stars[0],this.state.stars[1],this.state.stars[2],this.state.stars[3]);
    },200)
  }

  drawStars(){
      const stars = this.props.starCanvas;
      stars.fillStyle = "#FFF";
      let x= Math.floor((Math.random() * width) + 1);
      let y= Math.floor((Math.random() * height) + 1);
      let w= Math.floor((Math.random() * 2) + 1);
      this.state.stars.push(x,y,w,w)
      stars.fillRect(x,y,w,w);
  }

  render() {
    return(
        <div> </div>
    )
  }

}
