import React from 'react'
import Star from './star'
import Asteroide from './asteroide'
import Ship from './ship'



export default class Spaceio extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            canvas : null,
            screen: {
                width: window.innerWidth,
                height: window.innerHeight,
                ratio:  1
            },
            ship: null,
            keyDeleteShip: null,
            countIdTouchBullet: [],
            loose : false,
            level: 0,
            numberAsteroide: 1,
        }
    }

    componentDidMount = () =>{
        this.setState({
          canvas : this.refs.canvas.getContext('2d')
        })
        setInterval(() => {
        if(this.state.countIdTouchBullet.length === this.state.numberAsteroide){
          this.setState({countIdTouchBullet:[],numberAsteroide:this.state.numberAsteroide+2,level:this.state.level+1})
        }
      }, 3000)
    }

    didCollide = (asteroideX, asteroideY, asteroideHeight, shipX, shipY, shipHeight, key) => {
        if(this.state.ship !== null){
        if(this.state.countIdTouchBullet.length > 0){
            if(!this.state.countIdTouchBullet.includes(key)){
              let distance = Math.sqrt(Math.pow(shipX - asteroideX, 2) + Math.pow(shipY - asteroideY, 2))
              if(distance < 40) {
                this.setState({loose: true})
              }
            }
        }else{
           let distance = Math.sqrt(Math.pow(shipX - asteroideX, 2) + Math.pow(shipY - asteroideY, 2))
           if(distance < 40) {
             this.setState({loose: true})
           }
         }
       }
     }

    asteroides = (x,y,key) => {
        this.setState({keyDeleteShip: [x,y,key]})
        if(key===null) key=-1
        this.didCollide(x, y, 40, this.state.ship[0], this.state.ship[1], 20, key)
     }

     ship = (x,y) => {
            this.setState({
              ship: [x,y]
            })
          }

     bullets = (bulletsArray) => {
       if(bulletsArray.length !== 0){
         var unique = this.state.countIdTouchBullet.filter(function(item, k, ar){ return ar.indexOf(item) === k; });
         this.setState({countIdTouchBullet:unique})
         for (var i = 0; i < bulletsArray.length; i++) {
             for (var j = 0; j < this.state.keyDeleteShip.length; j++) {
               let distance = Math.sqrt(Math.pow(bulletsArray[i].x - this.state.keyDeleteShip[0], 2) + Math.pow(bulletsArray[i].y - this.state.keyDeleteShip[1], 2))
               if(distance < 40) {
                 this.state.countIdTouchBullet.push(this.state.keyDeleteShip[2])
               }
             }
           }
         }
     }

     render() {
      var allStars = [];
      for (var i = 0; i < 1; i++) {
        allStars.push(this.state.canvas && <Star key={i} starCanvas={this.state.canvas}/>);
      }

      var allAsteroides = [];
      for (var j = 0; j < this.state.numberAsteroide; j++) {
        allAsteroides.push(this.state.canvas && <Asteroide key={j} id={j} deleteShip={this.state.keyDeleteShip} idDelete={this.state.countIdTouchBullet} asteroideCanvas={this.state.canvas} asteroide={this.asteroides}/>);
      }

      const ship = this.state.canvas && <Ship shipCanvas={this.state.canvas} didLoose={this.state.loose} ship={this.ship} bullet={this.bullets}/>

      return (
              <div>
                <canvas ref="canvas"
                    width={this.state.screen.width * this.state.screen.ratio}
                    height={this.state.screen.height * this.state.screen.ratio}
                />
                <h1 id="overlay">{this.state.level}</h1>
                {allStars}
                {allAsteroides}
                {ship}
                {this.state.loose &&
                  <div class="form-style-5">
                  <form>
                  <fieldset>
                  <legend><span class="number"></span>Perdu, vous êtes allé jusqu'au niveau {this.state.level} </legend>
                  </fieldset>
                  <input type="submit" value="Recommencer" />
                  </form>
                  </div>
                }
              </div>
            )
    }
}
